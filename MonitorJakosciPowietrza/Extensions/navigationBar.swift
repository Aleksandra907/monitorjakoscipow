//
//  navigationBar.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 06.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 80.0)
    }
    
}
