//
//  Button.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 02.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit

extension UIButton {
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2.0)
        rotateAnimation.duration = duration
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

