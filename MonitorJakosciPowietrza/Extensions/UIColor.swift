//
//  UIColor.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 06.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct scaleOfAirQuality {
        static var bardzoDobry: UIColor  { return UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 1) }
        static var dobry: UIColor { return UIColor(red: 170/255, green: 255/255, blue: 0/255, alpha: 1) }
        static var umiarkowany: UIColor { return UIColor(red: 255/255, green: 255/255, blue: 0/255, alpha: 1) }
        static var dostateczny: UIColor { return UIColor(red: 255/255, green: 150/255, blue: 0/255, alpha: 1) }
        static var zly: UIColor { return UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1) }
        static var bardzoZly: UIColor { return UIColor(red: 170/255, green: 0/255, blue: 0/255, alpha: 1) }
    }
}
