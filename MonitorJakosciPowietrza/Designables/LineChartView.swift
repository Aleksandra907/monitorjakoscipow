//
//  LineChartView.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 06.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import UIKit
import Charts

@IBDesignable class DesignableChartView: LineChartView {
    
    @IBInspectable var rotation: Float = 0 {
        didSet {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(rotation * Float.pi/180));
        }
    }
    
    @IBInspectable var clipsToBoundsValue: Bool = true {
        didSet {
            self.clipsToBounds = clipsToBoundsValue
        }
    }
}

