//
//  DanaPomiarowa.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct MeasuringData: JSONDecodable{
    
    var values: [MeasuringDataValue]?
    
    init?(json: JSON){
        if let values: [MeasuringDataValue] = ("values" <~~ json) {
            self.values = values
        }else {
            self.values = nil
        }
    }
}
