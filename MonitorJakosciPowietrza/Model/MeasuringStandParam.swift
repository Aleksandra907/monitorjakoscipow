//
//  MeasuringStandParam.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct MeasuringStandParam: JSONDecodable{
    
    var paramName: String?    
    var paramFormula: String?

    init?(json: JSON){
        self.paramName = ("paramName" <~~ json)!
        self.paramFormula = ("paramFormula" <~~ json)!
    }
}
