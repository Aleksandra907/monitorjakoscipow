//
//  City.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct City: JSONDecodable{
    
    var commune: Comune?
    
    init?(json: JSON){
        if let commune: Comune = ("commune" <~~ json) {
            self.commune = commune
        }else {
            self.commune = nil
        }
    }
}
