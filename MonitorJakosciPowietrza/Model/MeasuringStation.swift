//
//  StacjaPomiarowa.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct MeasuringStation: JSONDecodable{
    
    var id: Int?
    var stationName: String?
    var city: City?
    
    init?(json: JSON){
        self.id = ("id" <~~ json)!
        self.stationName = ("stationName" <~~ json)!        
        if let city: City = ("city" <~~ json) {
            self.city = city
        }else {
            self.city = nil
        }
    }
}
