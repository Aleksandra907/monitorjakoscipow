//
//  IndeksJakosciPowietrza.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct AirQualityIndex: JSONDecodable{
    
    var stSourceDataDate: String?
    var stCalcDate: String?
    var stIndexLevel: IndexLevel?

    var so2SourceDataDate: String?
    var so2CalcDate: String?
    var so2IndexLevel: IndexLevel?
    
    var no2SourceDataDate: String?
    var no2CalcDate: String?
    var no2IndexLevel: IndexLevel?
    
    var coSourceDataDate: String?
    var coCalcDate: String?
    var coIndexLevel: IndexLevel?
    
    var pm10SourceDataDate: String?
    var pm10CalcDate: String?
    var pm10IndexLevel: IndexLevel?
    
    var pm25SourceDataDate: String?
    var pm25CalcDate: String?
    var pm25IndexLevel: IndexLevel?
    
    var o3SourceDataDate: String?
    var o3CalcDate: String?
    var o3IndexLevel: IndexLevel?
    
    var c6h6SourceDataDate: String?
    var c6h6CalcDate: String?
    var c6h6IndexLevel: IndexLevel?
    
    init?(json: JSON){
        if let stSourceDataDate: String = ("stSourceDataDate" <~~ json) {
            self.stSourceDataDate = stSourceDataDate
        }else {
            self.stSourceDataDate = nil
        }
        if let stCalcDate: String = ("stCalcDate" <~~ json) {
            self.stCalcDate = stCalcDate
        }else {
            self.stCalcDate = nil
        }
        if let stIndexLevel: IndexLevel = ("stIndexLevel" <~~ json) {
            self.stIndexLevel = stIndexLevel
        }else {
            self.stIndexLevel = nil
        }
        
        if let so2SourceDataDate: String = ("so2SourceDataDate" <~~ json) {
            self.so2SourceDataDate = so2SourceDataDate
        }else {
            self.so2SourceDataDate = nil
        }
        if let so2CalcDate: String = ("so2CalcDate" <~~ json) {
            self.so2CalcDate = so2CalcDate
        }else {
            self.so2CalcDate = nil
        }
        if let so2IndexLevel: IndexLevel = ("so2IndexLevel" <~~ json) {
            self.so2IndexLevel = so2IndexLevel
        }else {
            self.so2IndexLevel = nil
        }
        
        if let no2SourceDataDate: String = ("no2SourceDataDate" <~~ json) {
            self.no2SourceDataDate = no2SourceDataDate
        }else {
            self.no2SourceDataDate = nil
        }
        if let no2CalcDate: String = ("no2CalcDate" <~~ json) {
            self.no2CalcDate = no2CalcDate
        }else {
            self.no2CalcDate = nil
        }
        if let no2IndexLevel: IndexLevel = ("no2IndexLevel" <~~ json) {
            self.no2IndexLevel = no2IndexLevel
        }else {
            self.no2IndexLevel = nil
        }
        
        
        if let coSourceDataDate: String = ("coSourceDataDate" <~~ json) {
            self.coSourceDataDate = coSourceDataDate
        }else {
            self.coSourceDataDate = nil
        }
        if let coCalcDate: String = ("coCalcDate" <~~ json) {
            self.coCalcDate = coCalcDate
        }else {
            self.coCalcDate = nil
        }
        if let coIndexLevel: IndexLevel = ("coIndexLevel" <~~ json) {
            self.coIndexLevel = coIndexLevel
        }else {
            self.coIndexLevel = nil
        }
        
        
        if let pm10SourceDataDate: String = ("pm10SourceDataDate" <~~ json) {
            self.pm10SourceDataDate = pm10SourceDataDate
        }else {
            self.pm10SourceDataDate = nil
        }
        if let pm10CalcDate: String = ("pm10CalcDate" <~~ json) {
            self.pm10CalcDate = pm10CalcDate
        }else {
            self.pm10CalcDate = nil
        }
        if let pm10IndexLevel: IndexLevel = ("pm10IndexLevel" <~~ json) {
            self.pm10IndexLevel = pm10IndexLevel
        }else {
            self.pm10IndexLevel = nil
        }

        
        if let pm25SourceDataDate: String = ("pm25SourceDataDate" <~~ json) {
            self.pm25SourceDataDate = pm25SourceDataDate
        }else {
            self.pm25SourceDataDate = nil
        }
        if let pm25CalcDate: String = ("pm25CalcDate" <~~ json) {
            self.pm25CalcDate = pm25CalcDate
        }else {
            self.pm25CalcDate = nil
        }
        if let pm25IndexLevel: IndexLevel = ("pm25IndexLevel" <~~ json) {
            self.pm25IndexLevel = pm25IndexLevel
        }else {
            self.pm25IndexLevel = nil
        }
        
        
        if let o3CalcDate: String = ("o3CalcDate" <~~ json) {
            self.o3CalcDate = o3CalcDate
        }else {
            self.o3CalcDate = nil
        }
        if let o3CalcDate: String = ("o3CalcDate" <~~ json) {
            self.o3CalcDate = o3CalcDate
        }else {
            self.o3CalcDate = nil
        }
        if let o3IndexLevel: IndexLevel = ("o3IndexLevel" <~~ json) {
            self.o3IndexLevel = o3IndexLevel
        }else {
            self.o3IndexLevel = nil
        }
        
        
        if let c6h6SourceDataDate: String = ("c6h6SourceDataDate" <~~ json) {
            self.c6h6SourceDataDate = c6h6SourceDataDate
        }else {
            self.c6h6SourceDataDate = nil
        }
        if let c6h6CalcDate: String = ("c6h6CalcDate" <~~ json) {
            self.c6h6CalcDate = c6h6CalcDate
        }else {
            self.c6h6CalcDate = nil
        }
        if let c6h6IndexLevel: IndexLevel = ("c6h6IndexLevel" <~~ json) {
            self.c6h6IndexLevel = c6h6IndexLevel
        }else {
            self.c6h6IndexLevel = nil
        }
    }
    
}
