//
//  StanowiskoPomiarowe.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct MeasuringStand: JSONDecodable{
    
    var id: Int?
    var param: MeasuringStandParam?
    
    init?(json: JSON){
        self.id = ("id" <~~ json)!
        if let param: MeasuringStandParam = ("param" <~~ json) {
            self.param = param
        }else {
            self.param = nil
        }
    }
}
