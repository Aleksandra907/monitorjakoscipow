//
//  MeasuringDataValue.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct MeasuringDataValue: JSONDecodable{
    
    var date: String?
    var value: Float?
    
    init?(json: JSON){
        self.date = ("date" <~~ json)!
        if let value: Float = ("value" <~~ json) {
            self.value = value
        }else {
            self.value = nil
        }
    }
}
