//
//  Comune.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct Comune: JSONDecodable{
    
    var provinceName: String?    
    
    init?(json: JSON){
        self.provinceName = ("provinceName" <~~ json)!
    }
}
