//
//  StIndexLevel.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 01.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import Foundation
import Gloss

struct IndexLevel: JSONDecodable{
    
    var id: Int8?
    var indexLevelName: String?

    init?(json: JSON){
        self.id = ("id" <~~ json)!
        self.indexLevelName = ("indexLevelName" <~~ json)!
    }
}
