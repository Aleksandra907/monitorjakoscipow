//
//  DataViewController.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 06.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import Charts
import RappleProgressHUD

class DataViewController: UIViewController, refreshButtonProtocol, ChartViewDelegate {

    @IBOutlet weak var refreshButton: RefreshButton!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var refreshView: UIView!
    
    var measuringStand: MeasuringStand? = nil
    var airQualityIndex: AirQualityIndex? = nil
    
    let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [UIColor.cyan.cgColor, UIColor.clear.cgColor] as CFArray, locations: [0.5, 0.0])

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setChartOptions()
        getData()
        AppDelegate.AppUtility.lockOrientation(.portrait)
    }
    
    private func setUI () {
        self.navigationItem.title = measuringStand!.param!.paramName
        self.refreshButton.refreshButtonDelegate = self
    }
    
    private func setChartOptions() {
        chartView.chartDescription?.text = ""
        chartView.legend.enabled = false
        chartView.delegate = self
        chartView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - self.refreshView.frame.height - 80)
        self.chartView.xAxis.granularity = 1
        self.chartView.xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 15.0)!
        self.chartView.leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 15.0)!
        self.chartView.rightAxis.drawAxisLineEnabled = false
        self.chartView.rightAxis.drawLabelsEnabled = false
        self.chartView.rightAxis.drawGridLinesEnabled = false
    }
    
    private func drawChart(measuringData: MeasuringData) {
        var dates: [String] = []
        var values: [Float] = []
        for elem in measuringData.values! {
            if elem.date != nil && elem.value != nil {
                dates.append(elem.date!)
                values.append(elem.value!)
            }
        }

        var chartDataEntries: [ChartDataEntry] = []
        
        for i in 0..<dates.count {
            chartDataEntries.append(ChartDataEntry(x: Double(i), y: Double(values[i])))
        }
        
        let lineDataSet = LineChartDataSet(values: chartDataEntries, label: nil)
        lineDataSet.setColor(NSUIColor.black, alpha: 1)
        lineDataSet.valueTextColor = NSUIColor.black
        lineDataSet.circleHoleColor = NSUIColor.black
        lineDataSet.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0)
        lineDataSet.drawFilledEnabled = true
        let lineChartData = LineChartData(dataSet: lineDataSet)
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: dates)
        lineChartData.setValueFont(NSUIFont(name: "HelveticaNeue-Light", size: 15.0))
        chartView.data = lineChartData
    }
    
    private func showError(errorDescription: String) {
        let alertController = UIAlertController(title: "Error", message:
            errorDescription, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func refreshButtonSelected() {
        getData()
    }
}

//Http functions
extension DataViewController {
    private func getData() {
        RappleActivityIndicatorView.startAnimatingWithLabel("Processing...")
        
        let httpService = HttpService()
        httpService.sendRequest(url: Urls.measuringData + String(describing: self.measuringStand!.id!), method: .get) { (responseObject, errorDescription) in
            RappleActivityIndicatorView.stopAnimation()
            if errorDescription != nil {
                self.showError(errorDescription: errorDescription!)
            }else {
                let measuringData = MeasuringData(json: responseObject as! JSON)!
                self.drawChart(measuringData: measuringData)
                
            }
        }
    }
}

