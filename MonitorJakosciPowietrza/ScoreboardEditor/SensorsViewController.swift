//
//  SensorsViewController.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 04.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit
import Alamofire
import RxCocoa
import RxSwift
import Gloss
import RappleProgressHUD

class SensorsViewController: UIViewController, refreshButtonProtocol {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshButton: RefreshButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var disposeBag: DisposeBag = DisposeBag()
    var station: MeasuringStation? = nil
    let measuredSensors: Variable<[MeasuringStand]> = Variable([])
    var airQualityIndex: Variable<AirQualityIndex?> = Variable(nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = station?.stationName
        setUI()
        bindUI()
        getDataAirQuality()
    }

    private func setUI() {
        self.refreshButton.refreshButtonDelegate = self
    }
    
    private func setBackround(worstAirIndex: Int8) {
        switch worstAirIndex {
        case  _ where worstAirIndex < 2:
            self.backgroundImage.image = UIImage(named: "ladne2")
            break
        case _ where worstAirIndex < 4:
            self.backgroundImage.image = UIImage(named: "umiark")
            break
        case _ where worstAirIndex < 6:
            self.backgroundImage.image = UIImage(named: "smog")
            break
        default:
            break
        }
    }
    
    private func getElementByKey(sensor: MeasuringStand) -> Int8? {
        switch(sensor.param!.paramFormula!) {
        case "ST":
            return self.airQualityIndex.value!.stIndexLevel?.id
        case "SO2":
            return self.airQualityIndex.value!.so2IndexLevel?.id
        case "NO2":
            return self.airQualityIndex.value!.no2IndexLevel?.id
        case "CO":
            return self.airQualityIndex.value!.coIndexLevel?.id
        case "PM10":
            return self.airQualityIndex.value!.pm10IndexLevel?.id
        case "PM2.5":
            return self.airQualityIndex.value!.pm25IndexLevel?.id
        case "O3":
            return self.airQualityIndex.value!.o3IndexLevel?.id
        case "C6H6":
            return self.airQualityIndex.value!.c6h6IndexLevel?.id
        default:
            return 8
        }
    }
    
    private func getColorById(qualityId: Int8) -> UIColor {
        switch(qualityId) {
            case 0 : return UIColor.scaleOfAirQuality.bardzoDobry
            case 1 : return UIColor.scaleOfAirQuality.dobry
            case 2 : return UIColor.scaleOfAirQuality.umiarkowany
            case 3 : return UIColor.scaleOfAirQuality.dostateczny
            case 4 : return UIColor.scaleOfAirQuality.zly
            case 5 : return UIColor.scaleOfAirQuality.bardzoZly
            default: return UIColor.white
        }
    }
    
    private func showError(errorDescription: String) {
        let alertController = UIAlertController(title: "Error", message:
            errorDescription, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func refreshButtonSelected() {
        getDataAirQuality()
    }
}

//Rx functions
extension SensorsViewController {
    func bindUI() {
        self.tableView.register(UINib(nibName: String(describing: SensorsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SensorsTableViewCell.self))
        
        self.measuredSensors
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: String(describing: SensorsTableViewCell.self), cellType: SensorsTableViewCell.self)) {
                row, sensor, cell in
                let qualityId = self.getElementByKey(sensor: sensor)
                if qualityId != nil {
                    cell.backgroundColor = self.getColorById(qualityId: qualityId!)
                }
                cell.commonInit(sensor: sensor.param!.paramName!)
            }.disposed(by: disposeBag)
        
        self.airQualityIndex
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { (index) in
                if index != nil {
                    let tab = [index!.stIndexLevel?.id, index!.so2IndexLevel?.id, index!.no2IndexLevel?.id,
                               index!.coIndexLevel?.id, index!.pm10IndexLevel?.id, index!.pm25IndexLevel?.id,
                               index!.o3IndexLevel?.id, index!.c6h6IndexLevel?.id]
                    var worstIndex: Int8 = 0
                    for elem in tab {
                        if elem != nil {
                            if elem! > worstIndex {
                                worstIndex = elem!
                            }
                        }
                    }
                    self.setBackround(worstAirIndex: worstIndex)
                }
            }.disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(MeasuringStand.self)
            .subscribe(onNext: { [weak self] object in
                if let index = self?.tableView.indexPathForSelectedRow{
                    self?.tableView.deselectRow(at: index, animated: true)
                }
                let vc = self!.storyboard!.instantiateViewController(withIdentifier: "DataViewController") as! DataViewController
                vc.airQualityIndex = self?.airQualityIndex.value
                vc.measuringStand = object
                self!.navigationController!.pushViewController(vc, animated: true)
            })
            .disposed(by: disposeBag)
    }
}

//Http functions
extension SensorsViewController{
    private func getData() {
        let httpService = HttpService()
        httpService.sendRequest(url: Urls.measuringStands + String(describing: self.station!.id!), method: .get, completionHandler: { (responseData, errorDescription) in
            RappleActivityIndicatorView.stopAnimation()
            if errorDescription != nil {
                self.showError(errorDescription: errorDescription!)
            }else {
                self.measuredSensors.value = [MeasuringStand].from(jsonArray: responseData as! [JSON])!
            }
        })
    }
    
    private func getDataAirQuality() {
        RappleActivityIndicatorView.startAnimatingWithLabel("Processing...")
        
        let httpService = HttpService()
        httpService.sendRequest(url: Urls.airQualityIndex + String(describing: self.station!.id!), method: .get, completionHandler: { (responseData, errorDescription) in
            if errorDescription != nil {
                RappleActivityIndicatorView.stopAnimation()
                self.showError(errorDescription: errorDescription!)
            }else {
                self.airQualityIndex.value = AirQualityIndex(json: responseData as! JSON)!
                self.getData()
            }
        })
    }
}

