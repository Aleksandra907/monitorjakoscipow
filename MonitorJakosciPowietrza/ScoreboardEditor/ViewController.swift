//
//  ViewController.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 31.12.2017.
//  Copyright © 2017 Ola. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import RxCocoa
import RxSwift
import RxDataSources
import RappleProgressHUD

class ViewController: UIViewController, refreshButtonProtocol {
    
    @IBOutlet weak var refreshButton: RefreshButton!
    @IBOutlet weak var tableView: UITableView!
    
    var disposeBag: DisposeBag = DisposeBag()
    var sections: Variable<[SectionModel<String, MeasuringStation>]> = Variable([])

    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshButton.refreshButtonDelegate = self
        bindUI()
        getData()
    }
    
    func refreshButtonSelected() {
        getData()
    }
    
    private func showError(errorDescription: String) {
        let alertController = UIAlertController(title: "Error", message:
            errorDescription, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func sorterTableData(this:MeasuringStation, that:MeasuringStation) -> Bool {
        if(this.city?.commune?.provinceName == nil && that.city?.commune?.provinceName == nil) {
            
        }
        if(this.city?.commune?.provinceName == nil) {
            return false
        }
        if(that.city?.commune?.provinceName == nil) {
            return true
        }
        if this.city!.commune!.provinceName! == that.city!.commune!.provinceName! {
            return (this.stationName!).localizedCaseInsensitiveCompare(that.stationName!) == ComparisonResult.orderedAscending
        }
        return (this.city!.commune!.provinceName!).localizedCaseInsensitiveCompare(that.city!.commune!.provinceName!) == ComparisonResult.orderedAscending
    }
}

extension ViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect.zero)
        label.text = self.sections.value[section].model
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

// Rx functions
extension ViewController {
    func bindUI() {
        tableView.register(UINib(nibName: String(describing: StationsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: StationsTableViewCell.self))
        
        tableView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, MeasuringStation>>(
            configureCell: { (_, tv, indexPath, station: MeasuringStation) in
                let cell = tv.dequeueReusableCell(withIdentifier: String(describing: StationsTableViewCell.self)) as! StationsTableViewCell
                cell.commonInit(station: station.stationName!)
                return cell
        }
        )
        
        self.sections
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(MeasuringStation.self)
            .subscribe(onNext: { [weak self] object in
                if let index = self?.tableView.indexPathForSelectedRow{
                    self?.tableView.deselectRow(at: index, animated: true)
                }
                let vc = self!.storyboard!.instantiateViewController(withIdentifier: "SensorsViewController") as! SensorsViewController
                vc.station = object
                self!.navigationController!.pushViewController(vc, animated: true)
            })
            .disposed(by: disposeBag)
    }
}

//Http functions
extension ViewController {
    private func getData() {
        RappleActivityIndicatorView.startAnimatingWithLabel("Processing...")
        
        let httpService = HttpService()
        httpService.sendRequest(url: Urls.measuringStations, method: .get, completionHandler: {
            resposneData, errorDescription in
            
            RappleActivityIndicatorView.stopAnimation()
            if(errorDescription != nil) {
                self.showError(errorDescription: errorDescription!)
            }else {
                var stationsFromJSON = [MeasuringStation].from(jsonArray: resposneData as! [JSON])!
                stationsFromJSON.sort(by: self.sorterTableData(this:that:))
                
                var tempSections: [SectionModel<String, MeasuringStation>] = []
                var name = stationsFromJSON[0].city?.commune?.provinceName
                var list: [MeasuringStation] = []
                
                for i in 0...stationsFromJSON.count - 1 {
                    if name == stationsFromJSON[i].city?.commune?.provinceName {
                        list.append(stationsFromJSON[i])
                    }else {
                        tempSections.append(SectionModel(model: name!, items: list))
                        list = []
                        if stationsFromJSON[i].city?.commune?.provinceName == nil {
                            name = "Other:"
                        }else{
                            name = stationsFromJSON[i].city?.commune?.provinceName
                        }
                        list.append(stationsFromJSON[i])
                    }
                }
                
                self.sections.value = tempSections
            }
        })
    }
}


