//
//  StationsTableViewCell.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 02.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StationsTableViewCell: UITableViewCell {
    var disposeBag = DisposeBag()
    @IBOutlet weak var label: UILabel!
    
    func commonInit(station: String) { //placeNameLabel:String//MeasuringStation
        self.label.text = station
    }
    
    override func prepareForReuse() {
        self.disposeBag = DisposeBag()
    }
}
