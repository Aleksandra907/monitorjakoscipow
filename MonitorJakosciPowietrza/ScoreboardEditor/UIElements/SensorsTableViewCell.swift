//
//  SensorsTableViewCell.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 04.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SensorsTableViewCell: UITableViewCell {
    var disposeBag = DisposeBag()
    @IBOutlet weak var label: UILabel!
    
    func commonInit(sensor: String) { 
        self.label.text = sensor
    }
    
    override func prepareForReuse() {
        self.disposeBag = DisposeBag()
    }
}
