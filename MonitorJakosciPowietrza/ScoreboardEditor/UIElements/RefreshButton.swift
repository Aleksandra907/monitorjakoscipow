//
//  RefreshButton.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 02.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit

protocol refreshButtonProtocol {
    func refreshButtonSelected()
}

class RefreshButton: UIButton {

    var refreshButtonDelegate: refreshButtonProtocol!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.refreshButtonDelegate.refreshButtonSelected()
        
        UIView.animate(withDuration: 1.0, animations: {
            self.rotate360Degrees(duration: 1.0, completionDelegate: self)
        })
        
    }
}
