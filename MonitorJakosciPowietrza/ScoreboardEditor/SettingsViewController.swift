//
//  SettingsViewController.swift
//  MonitorJakosciPowietrza
//
//  Created by Ola on 07.01.2018.
//  Copyright © 2018 Ola. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsViewController: UIViewController {

    @IBOutlet weak var fromSB: UISearchBar!
    @IBOutlet weak var toSB: UISearchBar!
    @IBOutlet weak var saveButton: UIButton!
    
    var tableView: UITableView = UITableView()
    var fromSearchBarExpended: Bool = false
    var disposeBag: DisposeBag? = DisposeBag()
    
    var fromSearchBarText: Observable<String> {
        return fromSB
            .rx
            .text
            .map { $0! }
            .filter { $0.characters.count >= 0 }
            .throttle(1, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
    }
    
    var toSearchBarText: Observable<String> {
        return toSB
            .rx
            .text
            .map { $0! }
            .filter { $0.characters.count >= 0 }
            .throttle(1, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindUI()
    }

    func bindUI() {
        
        tableView.register(UINib(nibName: String(describing: SensorsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SensorsTableViewCell.self))
        
        
        self.filtreElements(for: fromSearchBarText)
            .drive(self.tableView.rx.items(cellIdentifier: String(describing: SensorsTableViewCell.self), cellType: SensorsTableViewCell.self)) {
                row, repository, cell in
                cell.commonInit(sensor: repository)
        }
        .disposed(by: disposeBag!)

        
        self.filtreElements(for: toSearchBarText)
            .drive(self.tableView.rx.items(cellIdentifier: String(describing: SensorsTableViewCell.self), cellType: SensorsTableViewCell.self)) {
                row, repository, cell in
                cell.commonInit(sensor: repository)
            }
            .disposed(by: disposeBag!)
        
        tableView
            .rx
            .modelSelected(String.self)
            .subscribe(onNext: { [weak self] text in
                if self!.fromSearchBarExpended {
                    self!.fromSB.text = text
                }else {
                    self!.toSB.text = text
                }
            })
            .disposed(by: disposeBag!)
    }
    
    
    @IBAction func saveButtonSelected(_ sender: Any) {
        
    }
    
    private func filtreElements(for observableText: Observable<String>) -> Driver<[String]> {
        return observableText
            .map { (text) -> [String] in
                var tempList: [String] = []
                for elem in ["dwad"] {
                    if text == "" || elem.contains(text) {
                        tempList.append(elem)
                    }
                }
                return tempList
            }
            .asDriver(onErrorJustReturn: [""])
    }
}


extension SettingsViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
}
